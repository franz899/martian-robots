const Planet = require('../Planet');

let planet = null

describe('Planet tests', () => {
  beforeAll(() => {
    planet = new Planet(5, 3);
  });

  it('can create a planet', () => {
    const planetTest = { x: 5, y: 3, scents: new Set() };
    expect(planet).toEqual(planetTest);
  });

  test('addScent', () => {
    const newScent = { x: 2, y: 6, orientation: 'S' };
    expect(planet.scents.size).toBe(0);
    planet.addScent(2, 6, 'S');
    expect(planet.scents.size).toBe(1);
  });

  it('should be a scent', () => {
   expect(planet.isThereAScent(2, 6, 'S')).toBe(true);
  });

  test('position within boundaries', ()=> {
    expect(planet.isPositionWithinBoundaries(3, 3)).toBe(true);
    expect(planet.isPositionWithinBoundaries(-1, 0)).toBe(false);
    expect(planet.isPositionWithinBoundaries(5, 3)).toBe(true);
  });
});