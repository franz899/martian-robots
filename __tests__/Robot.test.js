const Robot = require('../Robot');
const Planet = require('../Planet');

let robot = null;
let planet = null;

describe('Robot tests', () => {
  beforeAll(() => {
    robot = new Robot('3 2 W');
    planet = new Planet(5, 3);
  });

  it('should create a robot', () => {
    const robotTest = { x: 3, y: 2, orientation: 'W', lost: false };
    expect(robot.robot).toEqual(robotTest);
  });

  it('should get the status of the robot', () => {
    expect(robot.getStatus()).toBe('3 2 W');
  });

  test('return and set the new robot position', () => {
    const robotTest = { x: 3, y: 2, orientation: 'N', lost: false };
    const newPosition = robot.executeCommand('R');
    expect(newPosition).toEqual(robotTest);
    robot.updateRobotPosition(newPosition.x, newPosition.y, newPosition.orientation);
  });

  it('should move forward the robot', () => {
    const robotTest = { x: 3, y: 3, orientation: 'N', lost: false };
    const newPosition = robot.executeCommand('F');
    expect(newPosition).toEqual(robotTest);
    robot.updateRobotPosition(newPosition.x, newPosition.y, newPosition.orientation);
  });

  it('should set lost on the robot of out of boundaries', () => {
    const newPosition = robot.executeCommand('F');
    expect(planet.isPositionWithinBoundaries(newPosition.x, newPosition.y)).toBe(false);
    robot.lost();
    expect(robot.getStatus()).toEqual('3 3 N LOST');
  });
});