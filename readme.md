## Requirements

node v8 (tested on v8.4.0)


## Commands

### To run the instructions present in robot-inputs.txt
`` yarn start or npm start``

### To run the tests
`` yarn test or npm test``