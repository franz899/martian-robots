const fs = require('fs');
const Planet = require('./Planet');
const Robot = require('./Robot');

const inputFile = process.argv[2];

fs.readFile(inputFile, 'utf8', (err, data) => {
  if (err) throw err;
  const lines = data.split('\n').filter(l => l.length > 0);
  const planet = lines[0].split(' ');
  const robotsCommands = lines.slice(1);

  const Mars = new Planet(planet[0], planet[1]);

  let i = 0;
  while (robotsCommands[i]) {
    const robot = new Robot(robotsCommands[i]);
    const commands = robotsCommands[i+1];
    for (var j = 0; j < commands.length; j++) {
      const command = commands[j];
      const { x, y, orientation } = robot.executeCommand(command);
      if (Mars.isPositionWithinBoundaries(x, y)) {
        robot.updateRobotPosition(x, y, orientation);
      } else {
        const r = robot.getRobot();
        if (!Mars.isThereAScent(r.x, r.y, r.orientation)) {
          Mars.addScent(r.x, r.y, r.orientation);
          robot.lost();
          break;
        }
      }
    }
    console.log(robot.getStatus());
    i+=2
  }
});