class Robot {
  constructor(input) {
    const split = input.split(' ');

    this.robot = {
      x: parseInt(split[0]),
      y: parseInt(split[1]),
      orientation: split[2].toUpperCase(),
      lost: false,
    };
  }

  getRobot() {
    return this.robot;
  }

  getStatus() {
    const { x, y, orientation, lost } = this.robot;
    const lastPosition = `${x} ${y} ${orientation}`
    return lost ? `${lastPosition} LOST` : lastPosition;
  }

  lost() {
    this.robot = Object.assign({}, this.robot, { lost: true });
  }

  executeCommand(c) {
    switch (c) {
      case 'L':
      case 'R':
        return Object.assign(
          {},
          this.robot,
          {
            orientation: this.changeOrientation(c),
          }
        )
    
      case 'F':
        return Object.assign(
          {},
          this.robot,
          this.moveRobot(c),
        );
    }
  }
  
  changeOrientation(command) {
    const directions = ['N', 'E', 'S', 'W'];
    const lastIndex = directions.length-1;
    const oIndex = directions.indexOf(this.robot.orientation);
    let newIndex = null;
    switch (command) {
      case 'L':
        newIndex = oIndex === 0 ? lastIndex : oIndex - 1;
        break;
      case 'R':
        newIndex = oIndex === lastIndex ? 0 : oIndex + 1;
        break;
    }
    return directions[newIndex];
  }

  moveRobot() {
    const { x, y, orientation } = this.robot;
    switch (orientation) {
      case 'N': return { y: y+1 };
      case 'E': return { x: x+1 };
      case 'S': return { y: y-1 };
      case 'W': return { x: x-1 };
    }
  }

  updateRobotPosition(x, y, orientation) {
    this.robot = Object.assign(
      {},
      this.robot,
      { x, y, orientation },
    );
  }
}

module.exports = Robot;