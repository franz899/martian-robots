class Planet {
  constructor(x, y) {
    this.x = parseInt(x);
    this.y = parseInt(y);
    this.scents = new Set();
  }

  addScent(x, y, orientation) {
    this.scents.add(this.newScent(x, y, orientation));
  }
  
  newScent(x, y, orientation) {
    return `${x}-${y}-${orientation}`;
  }

  isThereAScent(x, y, orientation) {
    const scent = this.newScent(x, y, orientation);
    return this.scents.has(scent);
  }

  isWithinNegativeBoundaries(x, y) {
    return x > -1 && y > -1;
  }

  isWithinPositiveBoundaries(x, y) {
    return x <= this.x && y <= this.y;
  }

  isPositionWithinBoundaries(x, y) {
    return this.isWithinNegativeBoundaries(x,y)
      && this.isWithinPositiveBoundaries(x, y);
  }
}

module.exports = Planet;